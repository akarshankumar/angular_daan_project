var daanApp = angular.module('daanApp', ['ngRoute']);
daanApp.controller('DonorController', function ($scope, $log, donorFactory, $filter, $http, $location, $routeParams){
	$scope.donors=[];
    $scope.euButton='Edit';
    function init(){
        //$scope.donors = donorFactory.getDonors();
    $http({
            url: 'http://localhost:8080/MovieProductionDB/api/com.movieproductiondb.akactor',
            method: 'GET',
            params: {
                page: page,
                sortFields: $scope.sortInfo.fields[0],
                sortDirections: $scope.sortInfo.directions[0]
            }}).success(function(data) {
    $scope.donors = data;
    });
    };
	
	init();
	
    $log.log('Inside controller');
	
	$scope.deleteDonor = function(){
		for(i=0;i<$scope.donors.length;i++){
			$log.log($scope.donors[i].selected);
				$scope.donors = $filter('filter')($scope.donors, {selected: false})
		}
  };
	
    
	$scope.addDonor = function(){
    $scope.donors.push(
        {
            name:$scope.newDonor.name, 
            city:$scope.newDonor.city,
			selected:false,
			editable:false
        });
		$scope.newDonor.name = $scope.newDonor.city="";
};
	$scope.editDonor = function(){
        $log.log("Edit Donor");
		if($scope.euButton=='Edit'){
			$scope.euButton='Update'
		} else {
            $scope.tmpDonors = $filter('filter')($scope.donors, {editable: true});
				angular.forEach($scope.tmpDonors, function(tmpdonors,key) {
					$scope.tmpDonors[key].editable=false;
                    $scope.tmpDonors[key].selected=false;
			});
		
			$scope.euButton='Edit'
		}
		$scope.tmpDonors = $filter('filter')($scope.donors, {selected: true});
  		angular.forEach($scope.donors, function(donors,key) {
				angular.forEach($scope.tmpDonors, function(tmpdonors,key1) {
					if(donors.name==tmpdonors.name){
					$scope.donors[key].editable=true;
				}
			});
		});
		
  };
	
	
	/*$scope.isChecked = function(donorName){
	$scope.tmpDonors = $filter('filter')($scope.donors, {name: donorName});
	$scope.tmpDonors[0].editable=true;	
	console.log($scope.donors);
	};*/
});

daanApp.factory('donorFactory', function($log) {
    $log.log("Inside donorFactory");
    var donors = [
    {name: 'Akash', city: 'Bareilly', selected: false,editable:false},
    {name: 'Neha', city: 'Patna', selected: false,editable:false},
    {name: 'Kavya', city: 'Bangalore', selected: false,editable:false}
    ];
    
    var factory = {};
    
    factory.getDonors = function(){
      return donors;  
    };
    
    return factory;
}) 

daanApp.controller('RecipientController', function ($scope, $log, recipientFactory,$filter) {
	$scope.recipients=[];
    
    function init(){
        $scope.recipients = recipientFactory.getRecipients();
    };
	
	init();
	
    $log.log('Inside controller');
	
	$scope.deleteRecipient = function(index){
		//$log.log("Deleting" + index);
		for(i=0;i<$scope.recipients.length;i++){
			$log.log($scope.recipients[i].selected);
				$scope.recipients = $filter('filter')($scope.recipients, {selected: false})
		}
  };
    
	$scope.addRecipient = function(){
    $scope.recipients.push(
        {
            name:$scope.newRecipient.name, 
            city:$scope.newRecipient.city
        });
		$scope.newRecipient.name = $scope.newRecipient.city="";
};
});

daanApp.factory('recipientFactory', function($log) {
    $log.log("Inside recipientFactory");
    var recipients = [
    {name: 'Akash', city: 'Bareilly', selected: false},
    {name: 'Neha', city: 'Patna', selected: false},
    {name: 'Kavya', city: 'Bangalore', selected: false}
    ];
    
    var factory = {};
    
    factory.getRecipients = function(){
      return recipients;  
    };
    
    return factory;
}) 


daanApp.config(function($routeProvider){
$routeProvider
    .when('/',
          {
            //controller:'LoginController',
            templateUrl:'login.html'
          })
    .when('/donor',
          {
            controller:'DonorController',
            templateUrl:'partials/donor/donorHome.html'
          })
    .when('/recipient',
          {
            controller:'RecipientController',
            templateUrl:'partials/recipient/recipientHome.html'
          })
    .otherwise({redirectTo: '/'});
});


var users = [
    {name:'Akash', city:'Bareilly'},
    {name:'Neha', city:'Patna'},
    {name:'Kavya', city:'Bangalore'}
    ];